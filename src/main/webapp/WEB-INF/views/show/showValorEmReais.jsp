<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title></title>
</head>
<body>
<div>
    <c:set var="decimal" value="10.0"/>
    <fmt:setLocale value="pt-BR"/>
    <fmt:formatNumber var="valorEmReais" type="currency" value="${decimal}" currencySymbol="R$"/>
    <p>Valor em R$: <c:out value="${valorEmReais}"/></p>
</div>
</body>
</html>
