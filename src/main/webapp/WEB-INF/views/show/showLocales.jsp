<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="/css/estilo.css">
</head>
<body>
<div><h1>Installed Locales</h1></div>
<div>
    <c:forEach items="${locales}" var="locale">
        <p>${locale}</p>
    </c:forEach>
</div>
<script src="/js/jquery/jquery-3.6.0.min.js"></script>
</body>
</html>