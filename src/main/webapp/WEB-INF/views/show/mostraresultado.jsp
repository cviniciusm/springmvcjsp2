<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>mostraresultado</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/WEB-INF/css/estilo.css">
</head>
<body>
<div>
    <table>
        <tr>
            <td>Nome:</td>
            <td><c:out value="${pessoa.nome}"/></td>
        </tr>
        <tr>
            <td>Sobrenome:</td>
            <td><c:out value="${pessoa.sobreNome}"/></td>
        </tr>
        <tr>
            <td>Data de Nascimento:</td>
            <td><c:out value="${pessoa.dataNascimento}"/></td>
        </tr>
        <tr>
            <td>Idade:</td>
            <td><c:out value="${pessoa.idade}"/></td>
        </tr>
    </table>
</div>
<script src="${pageContext.request.contextPath}/WEB-INF/js/jquery/jquery-3.6.0.min.js"></script>
</body>
</html>