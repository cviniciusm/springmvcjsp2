<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
    <title>mostraformulario</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/WEB-INF/css/estilo.css">
</head>
<body>
<div>
    <form:form method="post" action="/processaformulario" modelAttribute="pessoa" enctype="multipart/form-data">
        <table>
            <tr>
                <td><form:label for="nome" path="nome">Nome:</form:label></td>
                <td><form:input type="text" id="nome" path="nome" /></td>
            </tr>
            <tr>
                <td><form:label for="sobreNome" path="sobreNome">Sobrenome:</form:label></td>
                <td><form:input type="text" id="sobreNome" path="sobreNome" /></td>
            </tr>
            <tr>
                <td><form:label for="dataNascimento" path="dataNascimento">Data de Nascimento (YYYY-MM-DD):</form:label></td>
                <td><form:input type="text" id="dataNascimento" path="dataNascimento" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="Enviar"/></td>
            </tr>
        </table>
    </form:form>
</div>
<script src="${pageContext.request.contextPath}/WEB-INF/js/jquery/jquery-3.6.0.min.js"></script>
</body>
</html>