package br.eti.cvm.springmvcjsp2.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.Period;

@Getter
@Setter
public class Pessoa {

    private String nome;

    private String sobreNome;

    private String dataNascimento;

    public Integer getIdade() {
        return Period.between(LocalDate.parse(dataNascimento), LocalDate.now()).getYears();
    }

}
