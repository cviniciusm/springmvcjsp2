package br.eti.cvm.springmvcjsp2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Springmvcjsp2Application extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Springmvcjsp2Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Springmvcjsp2Application.class, args);
    }

}
