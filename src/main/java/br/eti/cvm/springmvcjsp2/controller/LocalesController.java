package br.eti.cvm.springmvcjsp2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

@Controller
public class LocalesController {



    @GetMapping("/showLocales")
    String showLocales(ModelMap modelMap) {
        Locale[] localesArray = Locale.getAvailableLocales();
        List localesList = new ArrayList<String>();

        for(Locale l: localesArray)
            localesList.add(l.toString());

        modelMap.addAttribute("locales", localesList);

        return "/show/showLocales";
    }

}
