package br.eti.cvm.springmvcjsp2.controller;

import br.eti.cvm.springmvcjsp2.model.Pessoa;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping()
public class PessoaController {

    private Pessoa pessoa;

    @GetMapping("/mostraformulario")
    public String mostraFormulario(@ModelAttribute("pessoa") Pessoa pessoa) {
        return "/form/mostraformulario";
    }

    @PostMapping("/processaformulario")
    public ModelAndView processaResultado(Pessoa pessoa) {
        ModelAndView modelAndView = new ModelAndView("redirect:/mostraresultado");

        this.pessoa = pessoa;

        return modelAndView;
    }

    @GetMapping("/mostraresultado")
    public ModelAndView mostraResultado() {
        ModelAndView modelAndView = new ModelAndView("/show/mostraresultado");

        modelAndView.addObject("pessoa", this.pessoa);

        return modelAndView;
    }

}
