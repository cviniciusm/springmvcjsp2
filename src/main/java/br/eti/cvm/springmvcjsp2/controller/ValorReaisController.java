package br.eti.cvm.springmvcjsp2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ValorReaisController {

    @GetMapping("/showReais")
    String showValorEmReais() {
        return "/show/showValorEmReais";
    }
}
