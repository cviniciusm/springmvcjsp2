package br.eti.cvm.springmvcjsp2.controller;

import br.eti.cvm.springmvcjsp2.model.Usuario;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestScope
public class BemVindoController {

    @GetMapping({"","/"})
    public ModelAndView index() {
        Usuario usuario = new Usuario();
        usuario.setNome("USUARIO TESTE");

        List<String> continentes = new ArrayList<>();
        continentes.add("América do Norte");
        continentes.add("América do Sul");
        continentes.add("Ásia");
        continentes.add("Europa");
        continentes.add("África");
        continentes.add("Oceania");
        continentes.add("Ártico");
        continentes.add("Antártica");

        Collections.sort(continentes);

        ModelAndView modelAndView = new ModelAndView("bemvindo");
        modelAndView.addObject("usuario", usuario);
        modelAndView.addObject("continentes", continentes);

        return modelAndView;
    }

}
